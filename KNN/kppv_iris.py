# coding=utf-8

import matplotlib.pyplot as plot
from sklearn import neighbors, datasets, metrics
from sklearn.model_selection import train_test_split

#Load IRIS dataset 
iris = datasets.load_iris()
irisFeatures = iris.data[:, :] #Features for the different flowers
irisClass = iris.target #Flower class

#split values 80% to train and 20% to test
irisFeaturesTrain, irisFeaturesTest, irisClassTrain, irisClassTest = train_test_split(irisFeatures, irisClass, test_size=0.2, random_state=4)

k_range = range(1, 26)
score = {}
score_list = []

for k in k_range:
    knn=neighbors.KNeighborsClassifier(n_neighbors=k) 
    knn.fit(irisFeaturesTrain, irisClassTrain)
    irisClassPredicted = knn.predict(irisFeaturesTest)
    score[k] = metrics.accuracy_score(irisClassTest, irisClassPredicted)
    score_list.append(metrics.accuracy_score(irisClassTest, irisClassPredicted)*100)

plot.plot(k_range, score_list)
plot.xlabel('K neighbour')
plot.ylabel('Accuracy')
plot.show()

print(score_list)
