# coding=utf-8

#   Dense type multi-layer neural network on Iris example

import pandas
import numpy as np
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Activation
from sklearn.model_selection import train_test_split

#   Load data
iris = pandas.read_csv("iris.csv", header=None)
features = iris.values[:,:4].astype(float)
label = iris.values[:,4]

features_train, features_test, label_train, label_test = train_test_split(features, label, train_size=0.5, random_state=4)


def one_hot_encode_object_array(arr):
    uniques, ids = np.unique(arr, return_inverse=True) 
    return np_utils.to_categorical(ids, len(uniques))
label_train_ohe = one_hot_encode_object_array(label_train) 
label_test_ohe = one_hot_encode_object_array(label_test)

#   Setup the model 
model=Sequential()
model.add(Dense(30, input_dim=4, activation='relu'))
model.add(Dense(3, activation='softmax'))

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])

model.fit(features_train, label_train_ohe, nb_epoch=100, batch_size=1, verbose=1, validation_data=(features_test, label_test_ohe))

loss, accuracy = model.evaluate(features_test, label_test_ohe, verbose=1)
print("Accuracy = {:.2f}".format(accuracy))

model.summary()


