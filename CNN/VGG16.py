from keras.applications.vgg16 import preprocess_input, decode_predictions, VGG16 
from keras.preprocessing.image import load_img, img_to_array

model = VGG16()
img = load_img('broccoli.jpg', target_size=(224, 224))
img = img_to_array(img)
img = img.reshape((1, img.shape[0], img.shape[1], img.shape[2]))
imh = preprocess_input(img)
yhat = model.predict(img)
label = decode_predictions(yhat)
label = label[0][0]
print('%s (%.2f%%)' % (label[1], label[2]*100))