# coding=utf-8

import pandas
from keras.models import Sequential
from keras.layers import Dense, Activation
from sklearn.model_selection import train_test_split
import numpy as np
from keras.utils import np_utils
import matplotlib.pyplot as plt
import pathlib
import cv2
from skimage import color, io

NB_CLASS = 43
IMG_SIZE = 28


def imgShow(nbImage):
    plt.imshow(X[nbImage].reshape(IMG_SIZE, IMG_SIZE), cmap='gray')
    print(y[nbImage])
    plt.show()


# training
X = []
for i in range(0, NB_CLASS):
    if(i < 10):
        training_paths = pathlib.Path(
            '/home/david/cours/IA/ws/ai_basics/Data/GTSRB_train/Final_Training/Images/0000'+str(i)).glob("*.ppm")
    else:
        training_paths = pathlib.Path(
            '/home/david/cours/IA/ws/ai_basics/Data/GTSRB_train/Final_Training/Images/000'+str(i)).glob("*.ppm")

    sorted_path = sorted([x for x in training_paths])
    for p in range(0, len(sorted_path)):
        img = cv2.imread(str(sorted_path[p]), cv2.IMREAD_GRAYSCALE)
        img2 = np.asarray(cv2.resize(img, (IMG_SIZE, IMG_SIZE)))

        # print(img2.shape)
        # print(img2)
        X.append(img2.reshape(1, -1)[0]/255.0)

y = []
CLASS = [210, 2220, 2250, 1410, 1980, 1860, 420, 1440, 1410, 1470,
         2010, 1320, 2100, 2160, 780, 630, 420, 1110, 1200, 210, 360, 330, 390,
         510, 270, 1500, 600, 240, 540, 270, 450, 780, 240, 690, 420, 1200, 390, 
         210,2070, 300, 360, 240, 240 ]  # number of image per class (-1 for index)
CLASS[0] = np.asarray(CLASS)[0] - 1
cumsum = np.cumsum(CLASS)
print(cumsum)
#w = np.asarray(np.where(cumsum < 10))[0]

for i in range(0, len(X)):
    index = np.where(cumsum < i)[0]
    if len(index) == 0:
        y.append(0)
    else:
        y.append(np.max(index) + 1)

# imgShow(6089)
# imgShow(6090)
# print(np.cumsum(CLASS))
# print(len(X))
# print(len(y))

X = np.asarray(X)
y = np.asarray(y)
X_train, X_test, y_train, y_test = train_test_split(
    X, y, train_size=0.6, random_state=1)


def one_hot_encode_object_array(arr):
    uniques, ids = np.unique(arr, return_inverse=True)
    return np_utils.to_categorical(ids, len(uniques))

# def one_hot_encode_object_array(arr):
#     a = np.zeros(shape=(arr.shape[0], NB_CLASS))
#     for i in range(0, arr.shape[0]):
#         a[i][arr[i]] = 1.0
#     return a


y_train_ohe = one_hot_encode_object_array(y_train)
y_test_ohe = one_hot_encode_object_array(y_test)

print(X_train.shape)

# couches
model = Sequential()
model.add(Dense(30, input_dim=IMG_SIZE*IMG_SIZE, activation='relu'))
model.add(Dense(NB_CLASS, activation='softmax'))

# paramettrage
model.compile(optimizer="adam", loss="categorical_crossentropy",
              metrics=["accuracy"])

# apprentissage
model.fit(X_train, y_train_ohe, nb_epoch=100, batch_size=10,
          verbose=1, validation_data=(X_test, y_test_ohe))

loss, accuracy = model.evaluate(X_test, y_test_ohe, verbose=1)
print("Accuracy = {:.2f}".format(accuracy))

model.summary()
