# coding=utf-8

import time
from skimage.feature import hog
import pandas
import numpy as np
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Activation
from sklearn.model_selection import train_test_split

print("Start Loading...")
t0 = time.time()
mnist_train = np.asarray(pandas.read_csv("mnist_train.csv", header=None))
mnist_test = np.asarray(pandas.read_csv("mnist_test.csv", header=None))
t1 = time.time()
print("Loading time : %lfs" % (t1-t0))

mnist_train_features= mnist_train[:6000, 1:]
mnist_train_label = mnist_train[:6000, 0]
mnist_test_features= mnist_test[:1000, 1:]
mnist_test_label = mnist_test[:1000, 0]

def getListOfHOG(featureArray):
    listHOG = []
    for i in range(len(featureArray)):
        image = np.reshape(featureArray[i,:], (28,28))
        fd = hog(image, orientations=8, pixels_per_cell=(4, 4), cells_per_block=(1, 1))
        listHOG.append(fd)
        # print(np.asanyarray(listHOG).shape)
    return np.asanyarray(listHOG)

print("Start determining HOGs")
t0 = time.time()
featuresHog_train = getListOfHOG(mnist_train_features)
featuresHog_test = getListOfHOG(mnist_test_features)
t1 = time.time()
print("Determining HOGs duration : %d" % (t1-t0))

print("Shape HOG Train")
print(featuresHog_train.shape)
print("Shape HOG Test")
print(featuresHog_test.shape)

def one_hot_encode_object_array(arr):
    uniques, ids = np.unique(arr, return_inverse=True) 
    return np_utils.to_categorical(ids, len(uniques))

label_train = one_hot_encode_object_array(mnist_train_label)
label_test = one_hot_encode_object_array(mnist_test_label)

model=Sequential()
model.add(Dense(30, input_dim=392, activation='relu'))
model.add(Dense(20, input_dim=30, activation='relu'))
model.add(Dense(10, activation='softmax'))

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])

model.fit(featuresHog_train, label_train, nb_epoch=10, batch_size=10, verbose=1, validation_data=(featuresHog_test, label_test))

loss, accuracy = model.evaluate(featuresHog_test, label_test, verbose=1)
print("Accuracy = {:.2f}".format(accuracy))

model.summary()
