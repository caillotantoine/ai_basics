from keras.applications.vgg16 import preprocess_input, decode_predictions, VGG16 
from keras.preprocessing.image import load_img, img_to_array

model = VGG16() #load the model
img = load_img('/Users/davidqueruel/Documents/cours/IA/TP/CNN/wilson.jpg', target_size=(224,224)) #load an image from file
img = img_to_array(img) #convert the image pixels to a numpy array
img = img.reshape((1, img.shape[0], img.shape[1], img.shape[2])) # reshape data for the model
img = preprocess_input(img) # prepare the image for the VGG model
yhat = model.predict(img)# predict the probability across all output classes
label = decode_predictions(yhat) # convert the probabilities to class labels
label = label[0][0] # retrieve the most likely result, e.g. highest probability
print('%s (%.2f%%)' % (label[1], label[2]*100))             # print the classification