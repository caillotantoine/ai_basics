# coding=utf-8
from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib import pyplot

from sklearn.datasets.samples_generator import make_blobs
from pandas import DataFrame

#simuler un ensemble de données appartenant à 2 classes
X, y = make_blobs(n_samples=1000, centers=2, n_features=2, center_box=(-5,5))
df = DataFrame(dict(x=X[:,0], y=X[:,1], label=y))

colors = {0:'red', 1:'blue', 2:'green'}
fig, ax = pyplot.subplots()
grouped = df.groupby('label')
for key, group in grouped:
    group.plot(ax=ax, kind='scatter', x='x', y='y', label=key, color=colors[key])
#pyplot.show()
trainX, testX, trainy, testy = train_test_split(X, y, test_size=0.5, random_state=2)

model = LogisticRegression() #classificateur
model.fit(trainX, trainy) #adaptation du classifieur aux données
probs = model.predict_proba(testX) #
probs = probs[:, 1]
fpr, tpr, thresholds = roc_curve(testy, probs) #calcul courbe ROC
auc = roc_auc_score(testy, probs) #aire sous la courbe
print(thresholds)
print('AUC: %.3f' % auc)
pyplot.plot([0, 1], [0, 1], linestyle='--')
pyplot.plot(fpr, tpr, marker='.')
pyplot.show()



