# coding=utf-8
import numpy as np
from sklearn import svm
import pandas
import time
from sklearn.model_selection import train_test_split
import imageio
import pathlib
import matplotlib.pyplot as plt
import cv2
from skimage import color, io

NB_CLASS = 43
IMG_SIZE = 28

def imgShow(nbImage):
    plt.imshow(X[nbImage].reshape(IMG_SIZE, IMG_SIZE), cmap='gray')
    print(y[nbImage])
    plt.show()


# training
X = []
for i in range(0, NB_CLASS):
    if(i < 10):
        training_paths = pathlib.Path(
            '/home/david/cours/IA/ws/ai_basics/Data/GTSRB_train/Final_Training/Images/0000'+str(i)).glob("*.ppm")
    else:
        training_paths = pathlib.Path(
            '/home/david/cours/IA/ws/ai_basics/Data/GTSRB_train/Final_Training/Images/000'+str(i)).glob("*.ppm")

    sorted_path = sorted([x for x in training_paths])
    for p in range(0, len(sorted_path)):
        img = cv2.imread(str(sorted_path[p]), cv2.IMREAD_GRAYSCALE)
        img2 = np.asarray(cv2.resize( cv2.equalizeHist(img), (IMG_SIZE,IMG_SIZE) ))

        # print(img2.shape)
        # print(img2)
        # print("first")
        # print(img2)
        X.append(img2.reshape(1, -1)[0]/255.0)

y = []
CLASS = [210, 2220, 2250, 1410, 1980, 1860, 420, 1440, 1410, 1470,
         2010, 1320, 2100, 2160, 780, 630, 420, 1110, 1200, 210, 360, 330, 390,
         510, 270, 1500, 600, 240, 540, 270, 450, 780, 240, 690, 420, 1200, 390, 
         210,2070, 300, 360, 240, 240 ]  # number of image per class (-1 for index)
CLASS[0] = np.asarray(CLASS)[0] - 1
cumsum = np.cumsum(CLASS)
print(cumsum)
#w = np.asarray(np.where(cumsum < 10))[0]

for i in range(0, len(X)):
    index = np.where(cumsum < i)[0]
    if len(index) == 0:
        y.append(0)
    else:
        y.append(np.max(index) + 1)

X = np.asarray(X)
y = np.asarray(y)
X_app, X_tst, y_app, y_tst = train_test_split(X, y, train_size = 0.7, random_state = 1)
classifier=svm.SVC()  # creation du classifieur
#print(classifier)

# print(X[209].shape)
# print(X[209])
# imgShow(209)
# imgShow(210)

# imgShow(13199)
# imgShow(13200)

print(X.shape)

t0=time.time()
# adaptation des param. du classifieur (donnees d'appr.)
classifier.fit(X_app, y_app)
t1=time.time()
print('duree apprentissage : %.4fs' % (t1-t0))
# calcul des sorties du classifieur (donnees test)
predicted=classifier.predict(X_tst)
#print(predicted)
print(classifier.score(X_tst, y_tst))  # taux de reconnaissance
