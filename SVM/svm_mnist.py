import numpy as np
import pandas
import time
import matplotlib.pyplot as plot
from sklearn import svm, metrics


print("Start Loading...")
t0 = time.time()
mnist_train = np.asarray(pandas.read_csv("mnist_train.csv", header=None))
mnist_test = np.asarray(pandas.read_csv("mnist_test.csv", header=None))

t1 = time.time()
print("Loading time : %lfs" % (t1-t0))

scores = []
N_Range = range(1000, 10001, 1000)

for N in N_Range:
    print("================================\nN : %d" % N)
    mnist_train_features= mnist_train[:N, 1:]
    mnist_train_label = mnist_train[:N, 0]
    mnist_test_features= mnist_test[:500, 1:]
    mnist_test_label = mnist_test[:500, 0]

    mnist_train_features[mnist_train_features>0] = 1
    mnist_test_features[mnist_test_features>0] = 1


    # mnist_train_features = normalizeData(mnist_train_features_raw)
    # mnist_test_features = normalizeData(mnist_test_features_raw)


    classifier = svm.SVC(C=200,kernel='rbf',gamma=0.01,cache_size=8000,probability=False)
    # classifier = svm.SVC()
    print("Start learning")
    t0 = time.time()
    classifier.fit(mnist_train_features, mnist_train_label)
    t1 = time.time()
    print("Learning time : %lfs" % (t1-t0))
    t0 = time.time()
    predicted = classifier.predict(mnist_test_features)
    t1 = time.time()
    print("Testing time : %lfs" % (t1-t0))
    score = (metrics.accuracy_score(mnist_test_label, predicted)*100)
    print("Score : %.2lf%%" % score)

    print("Errors : \nPred \t Real")
    for i in range(len(predicted)-1):
        if predicted[i] != mnist_test_label[i]:
            print("%d \t %d" % (predicted[i], mnist_test_label[i]))
    # print(mnist_test_label)
    # print(predicted)
    scores.append(score)

plot.plot(N_Range, scores)
plot.xlabel("N Training vectors") 
plot.ylabel("Accuracy")
plot.show()
