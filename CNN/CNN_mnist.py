# coding=utf-8
import numpy as np
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
import pandas as pd
from keras.models import Sequential
from keras.layers import Conv2D, Flatten, Dense
import keras
rows, cols = 28, 28
nb_classes = 10
data = pd.read_csv('../mnist_train.csv').values 
x_train = data[:,1:].astype('float32')
y_train = np_utils.to_categorical(data[:,0].astype('int32'), nb_classes) 
data = pd.read_csv('../mnist_test.csv').values
x_test = data[:,1:].astype('float32')
y_test = np_utils.to_categorical(data[:,0].astype('int32'), nb_classes) 
x_train /= 255.0
x_test /= 255.0
x_train = x_train.reshape(x_train.shape[0], rows, cols, 1) 
x_test = x_test.reshape(x_test.shape[0], rows, cols, 1)

model = Sequential()
model.add(Conv2D(16, kernel_size=(3, 3), activation='relu',input_shape=(rows, cols, 1)))
model.add(Flatten())
model.add(Dense(nb_classes, activation='softmax'))
model.compile(loss=keras.losses.categorical_crossentropy, optimizer='adam', metrics=['accuracy'])

#définition de la structure du RNC paramétrage
#apprentissage / évaluation
model.fit(x_train, y_train, batch_size=128, epochs=30, verbose=1, validation_data=(x_test, y_test))