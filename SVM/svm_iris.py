# coding=utf-8

import numpy as np
from sklearn import svm, metrics
import pandas
from sklearn.model_selection import train_test_split
import time


iris = pandas.read_csv("/home/david/cours/IA/ws/ai_basics/Data/iris.csv", header=None)
irisFeatures = iris.values[:,:4] #Features for the different flowers
irisLabel = iris.values[:,4] #Flower class
irisFeaturesTrain, irisFeaturesTest, irisLabelTrain, irisLabelTest = train_test_split(irisFeatures, irisLabel, test_size=0.4, random_state=4)

classifier = svm.SVC()
print(classifier)
t0 = time.time()
classifier.fit(irisFeaturesTrain, irisLabelTrain)
t1 = time.time()
print('Durée d\'apprentissage : %lfs' % (t1-t0))

predicted = classifier.predict(irisFeaturesTest)
for flower in range(len(predicted)-1):
    print("predicted;real : %s:%s" % (predicted[flower], irisLabelTest[flower]))

print("Score : %.2lf%%" % (metrics.accuracy_score(irisLabelTest, predicted)*100))


